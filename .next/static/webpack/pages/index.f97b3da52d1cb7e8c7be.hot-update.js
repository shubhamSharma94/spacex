webpackHotUpdate_N_E("pages/index",{

/***/ "./components/molecule/Filter.js":
/*!***************************************!*\
  !*** ./components/molecule/Filter.js ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _atom_Button__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../atom/Button */ "./components/atom/Button.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! next/router */ "./node_modules/next/dist/client/router.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_2__);
var _this = undefined,
    _jsxFileName = "E:\\WEB\\Assingment\\spacex\\components\\molecule\\Filter.js",
    _s = $RefreshSig$();


var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;



var YEARS = ['2006', '2007', '2008', '2009', '2010', '2011', '2012', '2013', '2014', '2015', '2016', '2017', '2018', '2019', '2020']; // class Filter extends Component {
//   state = {
//     selectedFilter: [],
//   }
//   componentDidUpdate = () => {
//     console.log('mount')
//   }
//   render () {
//   }
// }

var Filter = function Filter(props) {
  _s();

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(''),
      selectedYear = _useState[0],
      setSelectedYear = _useState[1];

  var _useState2 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(''),
      selectedLaunch = _useState2[0],
      setSelectedLaunch = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(''),
      selectedLand = _useState3[0],
      setSelectedland = _useState3[1];

  var router = Object(next_router__WEBPACK_IMPORTED_MODULE_2__["useRouter"])();

  var handleClick = function handleClick(data, type) {
    if (type === 'launch_year') {
      setSelectedYear(data);
    } else if (type === 'launch_success') {
      setSelectedLaunch(data);
    } else {
      setSelectedland(data);
    }
  };

  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    var urlQuery = [selectedYear, selectedLaunch, selectedLand];
    var url = "/";
    urlQuery && urlQuery.map(function (elm, i) {
      if (elm) {
        if (i === 0) {
          url = url.concat("?".concat(elm));
        } else {
          url = url.concat("&".concat(elm));
        }
      }
    });
    router.push(url, undefined, {
      shallow: false
    });
  });
  return __jsx("div", {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 71,
      columnNumber: 5
    }
  }, "Filters:", __jsx("p", {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 73,
      columnNumber: 7
    }
  }, "Launch Year"), __jsx("div", {
    className: "row",
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 74,
      columnNumber: 7
    }
  }, YEARS && YEARS.map(function (year, i) {
    return __jsx("div", {
      key: i,
      className: "col-xs-6",
      __self: _this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 78,
        columnNumber: 15
      }
    }, __jsx(_atom_Button__WEBPACK_IMPORTED_MODULE_1__["default"], {
      className: "primary",
      label: year,
      action: "launch_year=".concat(year),
      type: "launch_year",
      handleClick: handleClick,
      __self: _this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 79,
        columnNumber: 17
      }
    }));
  })), __jsx("p", {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 89,
      columnNumber: 7
    }
  }, "Successful Launch"), __jsx("div", {
    className: "row",
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 90,
      columnNumber: 7
    }
  }, __jsx("div", {
    className: "col-xs-6",
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 91,
      columnNumber: 9
    }
  }, __jsx(_atom_Button__WEBPACK_IMPORTED_MODULE_1__["default"], {
    className: "primary",
    label: 'true',
    action: "launch_success=".concat(true),
    type: "launch_success",
    handleClick: handleClick,
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 92,
      columnNumber: 11
    }
  })), __jsx("div", {
    className: "col-xs-6",
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 94,
      columnNumber: 9
    }
  }, __jsx(_atom_Button__WEBPACK_IMPORTED_MODULE_1__["default"], {
    className: "secondary",
    label: 'false',
    action: "launch_success=".concat(false),
    type: "launch_success",
    handleClick: handleClick,
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 95,
      columnNumber: 11
    }
  }))), __jsx("p", {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 99,
      columnNumber: 7
    }
  }, "Successful landing"), __jsx("div", {
    className: "row",
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 100,
      columnNumber: 7
    }
  }, __jsx("div", {
    className: "col-xs-6",
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 101,
      columnNumber: 9
    }
  }, __jsx(_atom_Button__WEBPACK_IMPORTED_MODULE_1__["default"], {
    className: "primary",
    label: 'true',
    action: "land_success=".concat(true),
    type: "land_success",
    handleClick: handleClick,
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 102,
      columnNumber: 11
    }
  })), __jsx("div", {
    className: "col-xs-6",
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 104,
      columnNumber: 9
    }
  }, __jsx(_atom_Button__WEBPACK_IMPORTED_MODULE_1__["default"], {
    className: "secondary",
    label: 'false',
    action: "land_success=".concat(false),
    type: "land_success",
    handleClick: handleClick,
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 105,
      columnNumber: 11
    }
  }))));
};

_s(Filter, "TLPkP0vG33S7BRsBru/k6RZ8N/U=", false, function () {
  return [next_router__WEBPACK_IMPORTED_MODULE_2__["useRouter"]];
});

_c = Filter;
/* harmony default export */ __webpack_exports__["default"] = (Filter);

var _c;

$RefreshReg$(_c, "Filter");

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vY29tcG9uZW50cy9tb2xlY3VsZS9GaWx0ZXIuanMiXSwibmFtZXMiOlsiWUVBUlMiLCJGaWx0ZXIiLCJwcm9wcyIsInVzZVN0YXRlIiwic2VsZWN0ZWRZZWFyIiwic2V0U2VsZWN0ZWRZZWFyIiwic2VsZWN0ZWRMYXVuY2giLCJzZXRTZWxlY3RlZExhdW5jaCIsInNlbGVjdGVkTGFuZCIsInNldFNlbGVjdGVkbGFuZCIsInJvdXRlciIsInVzZVJvdXRlciIsImhhbmRsZUNsaWNrIiwiZGF0YSIsInR5cGUiLCJ1c2VFZmZlY3QiLCJ1cmxRdWVyeSIsInVybCIsIm1hcCIsImVsbSIsImkiLCJjb25jYXQiLCJwdXNoIiwidW5kZWZpbmVkIiwic2hhbGxvdyIsInllYXIiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQSxJQUFNQSxLQUFLLEdBQUcsQ0FDWixNQURZLEVBRVosTUFGWSxFQUdaLE1BSFksRUFJWixNQUpZLEVBS1osTUFMWSxFQU1aLE1BTlksRUFPWixNQVBZLEVBUVosTUFSWSxFQVNaLE1BVFksRUFVWixNQVZZLEVBV1osTUFYWSxFQVlaLE1BWlksRUFhWixNQWJZLEVBY1osTUFkWSxFQWVaLE1BZlksQ0FBZCxDLENBa0JBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBSUE7QUFFQTtBQUNBOztBQUdBLElBQU1DLE1BQU0sR0FBRyxTQUFUQSxNQUFTLENBQUNDLEtBQUQsRUFBVztBQUFBOztBQUFBLGtCQUNnQkMsc0RBQVEsQ0FBQyxFQUFELENBRHhCO0FBQUEsTUFDakJDLFlBRGlCO0FBQUEsTUFDSEMsZUFERzs7QUFBQSxtQkFFb0JGLHNEQUFRLENBQUMsRUFBRCxDQUY1QjtBQUFBLE1BRWpCRyxjQUZpQjtBQUFBLE1BRURDLGlCQUZDOztBQUFBLG1CQUdnQkosc0RBQVEsQ0FBQyxFQUFELENBSHhCO0FBQUEsTUFHakJLLFlBSGlCO0FBQUEsTUFHSEMsZUFIRzs7QUFJeEIsTUFBTUMsTUFBTSxHQUFHQyw2REFBUyxFQUF4Qjs7QUFDQSxNQUFNQyxXQUFXLEdBQUcsU0FBZEEsV0FBYyxDQUFDQyxJQUFELEVBQU9DLElBQVAsRUFBZ0I7QUFDbEMsUUFBSUEsSUFBSSxLQUFLLGFBQWIsRUFBNEI7QUFDMUJULHFCQUFlLENBQUNRLElBQUQsQ0FBZjtBQUNELEtBRkQsTUFFTyxJQUFJQyxJQUFJLEtBQUssZ0JBQWIsRUFBK0I7QUFDcENQLHVCQUFpQixDQUFDTSxJQUFELENBQWpCO0FBQ0QsS0FGTSxNQUVBO0FBQ0xKLHFCQUFlLENBQUNJLElBQUQsQ0FBZjtBQUNEO0FBRUYsR0FURDs7QUFVQUUseURBQVMsQ0FBQyxZQUFNO0FBQ2QsUUFBSUMsUUFBUSxHQUFHLENBQUNaLFlBQUQsRUFBZUUsY0FBZixFQUErQkUsWUFBL0IsQ0FBZjtBQUNBLFFBQUlTLEdBQUcsR0FBRyxHQUFWO0FBQ0FELFlBQVEsSUFBSUEsUUFBUSxDQUFDRSxHQUFULENBQWEsVUFBQ0MsR0FBRCxFQUFNQyxDQUFOLEVBQVk7QUFDbkMsVUFBSUQsR0FBSixFQUFTO0FBQ1AsWUFBSUMsQ0FBQyxLQUFLLENBQVYsRUFBYTtBQUNYSCxhQUFHLEdBQUdBLEdBQUcsQ0FBQ0ksTUFBSixZQUFlRixHQUFmLEVBQU47QUFDRCxTQUZELE1BRU87QUFDTEYsYUFBRyxHQUFHQSxHQUFHLENBQUNJLE1BQUosWUFBZUYsR0FBZixFQUFOO0FBQ0Q7QUFDRjtBQUVGLEtBVFcsQ0FBWjtBQVVBVCxVQUFNLENBQUNZLElBQVAsQ0FBWUwsR0FBWixFQUFpQk0sU0FBakIsRUFBNEI7QUFBRUMsYUFBTyxFQUFFO0FBQVgsS0FBNUI7QUFDRCxHQWRRLENBQVQ7QUFnQkEsU0FDRTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQUVFO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsbUJBRkYsRUFHRTtBQUFLLGFBQVMsRUFBQyxLQUFmO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsS0FFSXhCLEtBQUssSUFBSUEsS0FBSyxDQUFDa0IsR0FBTixDQUFVLFVBQUNPLElBQUQsRUFBT0wsQ0FBUCxFQUFhO0FBQzlCLFdBQ0U7QUFBSyxTQUFHLEVBQUVBLENBQVY7QUFBYSxlQUFTLEVBQUMsVUFBdkI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxPQUNFLE1BQUMsb0RBQUQ7QUFDRSxlQUFTLEVBQUMsU0FEWjtBQUVFLFdBQUssRUFBRUssSUFGVDtBQUVlLFlBQU0sd0JBQWlCQSxJQUFqQixDQUZyQjtBQUdFLFVBQUksRUFBQyxhQUhQO0FBR3FCLGlCQUFXLEVBQUViLFdBSGxDO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsTUFERixDQURGO0FBUUQsR0FUUSxDQUZiLENBSEYsRUFrQkU7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSx5QkFsQkYsRUFtQkU7QUFBSyxhQUFTLEVBQUMsS0FBZjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEtBQ0U7QUFBSyxhQUFTLEVBQUMsVUFBZjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEtBQ0UsTUFBQyxvREFBRDtBQUFRLGFBQVMsRUFBQyxTQUFsQjtBQUE0QixTQUFLLEVBQUUsTUFBbkM7QUFBMkMsVUFBTSwyQkFBb0IsSUFBcEIsQ0FBakQ7QUFBNkUsUUFBSSxFQUFDLGdCQUFsRjtBQUFtRyxlQUFXLEVBQUVBLFdBQWhIO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsSUFERixDQURGLEVBSUU7QUFBSyxhQUFTLEVBQUMsVUFBZjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEtBQ0UsTUFBQyxvREFBRDtBQUFRLGFBQVMsRUFBQyxXQUFsQjtBQUE4QixTQUFLLEVBQUUsT0FBckM7QUFBOEMsVUFBTSwyQkFBb0IsS0FBcEIsQ0FBcEQ7QUFBaUYsUUFBSSxFQUFDLGdCQUF0RjtBQUF1RyxlQUFXLEVBQUVBLFdBQXBIO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsSUFERixDQUpGLENBbkJGLEVBNEJFO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsMEJBNUJGLEVBNkJFO0FBQUssYUFBUyxFQUFDLEtBQWY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxLQUNFO0FBQUssYUFBUyxFQUFDLFVBQWY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxLQUNFLE1BQUMsb0RBQUQ7QUFBUSxhQUFTLEVBQUMsU0FBbEI7QUFBNEIsU0FBSyxFQUFFLE1BQW5DO0FBQTJDLFVBQU0seUJBQWtCLElBQWxCLENBQWpEO0FBQTJFLFFBQUksRUFBQyxjQUFoRjtBQUFnRyxlQUFXLEVBQUVBLFdBQTdHO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsSUFERixDQURGLEVBSUU7QUFBSyxhQUFTLEVBQUMsVUFBZjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEtBQ0UsTUFBQyxvREFBRDtBQUFRLGFBQVMsRUFBQyxXQUFsQjtBQUE4QixTQUFLLEVBQUUsT0FBckM7QUFBOEMsVUFBTSx5QkFBa0IsS0FBbEIsQ0FBcEQ7QUFBK0UsUUFBSSxFQUFDLGNBQXBGO0FBQW9HLGVBQVcsRUFBRUEsV0FBakg7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxJQURGLENBSkYsQ0E3QkYsQ0FERjtBQXdDRCxDQXZFRDs7R0FBTVgsTTtVQUlXVSxxRDs7O0tBSlhWLE07QUF5RVNBLHFFQUFmIiwiZmlsZSI6InN0YXRpYy93ZWJwYWNrL3BhZ2VzL2luZGV4LmY5N2IzZGE1MmQxY2I3ZThjN2JlLmhvdC11cGRhdGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyB1c2VTdGF0ZSwgdXNlRWZmZWN0IH0gZnJvbSAncmVhY3QnO1xyXG5pbXBvcnQgQnV0dG9uIGZyb20gXCIuLi9hdG9tL0J1dHRvblwiO1xyXG5pbXBvcnQgeyB1c2VSb3V0ZXIgfSBmcm9tICduZXh0L3JvdXRlcic7XHJcbmNvbnN0IFlFQVJTID0gW1xyXG4gICcyMDA2JyxcclxuICAnMjAwNycsXHJcbiAgJzIwMDgnLFxyXG4gICcyMDA5JyxcclxuICAnMjAxMCcsXHJcbiAgJzIwMTEnLFxyXG4gICcyMDEyJyxcclxuICAnMjAxMycsXHJcbiAgJzIwMTQnLFxyXG4gICcyMDE1JyxcclxuICAnMjAxNicsXHJcbiAgJzIwMTcnLFxyXG4gICcyMDE4JyxcclxuICAnMjAxOScsXHJcbiAgJzIwMjAnLFxyXG5dO1xyXG5cclxuLy8gY2xhc3MgRmlsdGVyIGV4dGVuZHMgQ29tcG9uZW50IHtcclxuLy8gICBzdGF0ZSA9IHtcclxuLy8gICAgIHNlbGVjdGVkRmlsdGVyOiBbXSxcclxuLy8gICB9XHJcblxyXG4vLyAgIGNvbXBvbmVudERpZFVwZGF0ZSA9ICgpID0+IHtcclxuLy8gICAgIGNvbnNvbGUubG9nKCdtb3VudCcpXHJcbi8vICAgfVxyXG5cclxuICBcclxuXHJcbi8vICAgcmVuZGVyICgpIHtcclxuICAgXHJcbi8vICAgfVxyXG4vLyB9XHJcblxyXG5cclxuY29uc3QgRmlsdGVyID0gKHByb3BzKSA9PiB7XHJcbiAgY29uc3QgW3NlbGVjdGVkWWVhciwgc2V0U2VsZWN0ZWRZZWFyXSA9IHVzZVN0YXRlKCcnKTtcclxuICBjb25zdCBbc2VsZWN0ZWRMYXVuY2gsIHNldFNlbGVjdGVkTGF1bmNoXSA9IHVzZVN0YXRlKCcnKTtcclxuICBjb25zdCBbc2VsZWN0ZWRMYW5kLCBzZXRTZWxlY3RlZGxhbmRdID0gdXNlU3RhdGUoJycpO1xyXG4gIGNvbnN0IHJvdXRlciA9IHVzZVJvdXRlcigpO1xyXG4gIGNvbnN0IGhhbmRsZUNsaWNrID0gKGRhdGEsIHR5cGUpID0+IHtcclxuICAgIGlmICh0eXBlID09PSAnbGF1bmNoX3llYXInKSB7XHJcbiAgICAgIHNldFNlbGVjdGVkWWVhcihkYXRhKTtcclxuICAgIH0gZWxzZSBpZiAodHlwZSA9PT0gJ2xhdW5jaF9zdWNjZXNzJykge1xyXG4gICAgICBzZXRTZWxlY3RlZExhdW5jaChkYXRhKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHNldFNlbGVjdGVkbGFuZChkYXRhKTtcclxuICAgIH1cclxuICAgIFxyXG4gIH1cclxuICB1c2VFZmZlY3QoKCkgPT4ge1xyXG4gICAgbGV0IHVybFF1ZXJ5ID0gW3NlbGVjdGVkWWVhciwgc2VsZWN0ZWRMYXVuY2gsIHNlbGVjdGVkTGFuZF07XHJcbiAgICBsZXQgdXJsID0gXCIvXCI7XHJcbiAgICB1cmxRdWVyeSAmJiB1cmxRdWVyeS5tYXAoKGVsbSwgaSkgPT4ge1xyXG4gICAgICBpZiAoZWxtKSB7XHJcbiAgICAgICAgaWYgKGkgPT09IDApIHtcclxuICAgICAgICAgIHVybCA9IHVybC5jb25jYXQoYD8ke2VsbX1gKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgdXJsID0gdXJsLmNvbmNhdChgJiR7ZWxtfWApO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgICBcclxuICAgIH0pXHJcbiAgICByb3V0ZXIucHVzaCh1cmwsIHVuZGVmaW5lZCwgeyBzaGFsbG93OiBmYWxzZSB9KVxyXG4gIH0pO1xyXG5cclxuICByZXR1cm4gKFxyXG4gICAgPGRpdj5cclxuICAgICAgRmlsdGVyczpcclxuICAgICAgPHA+TGF1bmNoIFllYXI8L3A+XHJcbiAgICAgIDxkaXYgY2xhc3NOYW1lPSdyb3cnPlxyXG4gICAgICAgIHtcclxuICAgICAgICAgIFlFQVJTICYmIFlFQVJTLm1hcCgoeWVhciwgaSkgPT4ge1xyXG4gICAgICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICAgIDxkaXYga2V5PXtpfSBjbGFzc05hbWU9J2NvbC14cy02Jz5cclxuICAgICAgICAgICAgICAgIDxCdXR0b24gXHJcbiAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT0ncHJpbWFyeScgXHJcbiAgICAgICAgICAgICAgICAgIGxhYmVsPXt5ZWFyfSBhY3Rpb249e2BsYXVuY2hfeWVhcj0ke3llYXJ9YH0gXHJcbiAgICAgICAgICAgICAgICAgIHR5cGU9J2xhdW5jaF95ZWFyJyBoYW5kbGVDbGljaz17aGFuZGxlQ2xpY2t9Lz5cclxuICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgKVxyXG4gICAgICAgICAgfSlcclxuICAgICAgICB9XHJcbiAgICAgIDwvZGl2PlxyXG5cclxuICAgICAgPHA+U3VjY2Vzc2Z1bCBMYXVuY2g8L3A+XHJcbiAgICAgIDxkaXYgY2xhc3NOYW1lPSdyb3cnPlxyXG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPSdjb2wteHMtNic+XHJcbiAgICAgICAgICA8QnV0dG9uIGNsYXNzTmFtZT0ncHJpbWFyeScgbGFiZWw9eyd0cnVlJ30gYWN0aW9uPXtgbGF1bmNoX3N1Y2Nlc3M9JHt0cnVlfWB9IHR5cGU9J2xhdW5jaF9zdWNjZXNzJyBoYW5kbGVDbGljaz17aGFuZGxlQ2xpY2t9IC8+XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPGRpdiBjbGFzc05hbWU9J2NvbC14cy02Jz5cclxuICAgICAgICAgIDxCdXR0b24gY2xhc3NOYW1lPSdzZWNvbmRhcnknIGxhYmVsPXsnZmFsc2UnfSBhY3Rpb249e2BsYXVuY2hfc3VjY2Vzcz0ke2ZhbHNlfWB9IHR5cGU9J2xhdW5jaF9zdWNjZXNzJyBoYW5kbGVDbGljaz17aGFuZGxlQ2xpY2t9Lz5cclxuICAgICAgICA8L2Rpdj5cclxuICAgICAgPC9kaXY+XHJcblxyXG4gICAgICA8cD5TdWNjZXNzZnVsIGxhbmRpbmc8L3A+XHJcbiAgICAgIDxkaXYgY2xhc3NOYW1lPSdyb3cnPlxyXG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPSdjb2wteHMtNic+XHJcbiAgICAgICAgICA8QnV0dG9uIGNsYXNzTmFtZT0ncHJpbWFyeScgbGFiZWw9eyd0cnVlJ30gYWN0aW9uPXtgbGFuZF9zdWNjZXNzPSR7dHJ1ZX1gfSB0eXBlPSdsYW5kX3N1Y2Nlc3MnICBoYW5kbGVDbGljaz17aGFuZGxlQ2xpY2t9Lz5cclxuICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT0nY29sLXhzLTYnPlxyXG4gICAgICAgICAgPEJ1dHRvbiBjbGFzc05hbWU9J3NlY29uZGFyeScgbGFiZWw9eydmYWxzZSd9IGFjdGlvbj17YGxhbmRfc3VjY2Vzcz0ke2ZhbHNlfWB9IHR5cGU9J2xhbmRfc3VjY2VzcycgIGhhbmRsZUNsaWNrPXtoYW5kbGVDbGlja30vPlxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgICA8L2Rpdj5cclxuICAgIDwvZGl2PlxyXG4gIClcclxufVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgRmlsdGVyO1xyXG5cclxuIl0sInNvdXJjZVJvb3QiOiIifQ==