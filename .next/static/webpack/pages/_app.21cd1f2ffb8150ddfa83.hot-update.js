webpackHotUpdate_N_E("pages/_app",{

/***/ "./components/molecule/Filter.js":
/*!***************************************!*\
  !*** ./components/molecule/Filter.js ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _atom_Button__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../atom/Button */ "./components/atom/Button.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! next/router */ "./node_modules/next/dist/client/router.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_2__);
var _this = undefined,
    _jsxFileName = "E:\\WEB\\Assingment\\spacex\\components\\molecule\\Filter.js",
    _s = $RefreshSig$();


var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;



var YEARS = ['2006', '2007', '2008', '2009', '2010', '2011', '2012', '2013', '2014', '2015', '2016', '2017', '2018', '2019', '2020'];

var Filter = function Filter(props) {
  _s();

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(''),
      selectedYear = _useState[0],
      setSelectedYear = _useState[1];

  var _useState2 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(''),
      selectedLaunch = _useState2[0],
      setSelectedLaunch = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(''),
      selectedLand = _useState3[0],
      setSelectedland = _useState3[1];

  var router = Object(next_router__WEBPACK_IMPORTED_MODULE_2__["useRouter"])();

  var handleClick = function handleClick(data, type) {
    if (type === 'launch_year') {
      setSelectedYear(data);
    } else if (type === 'launch_success') {
      setSelectedLaunch(data);
    } else {
      setSelectedland(data);
    }
  };

  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    var urlQuery = [selectedYear, selectedLaunch, selectedLand];
    var url = "/";
    urlQuery && urlQuery.map(function (elm, i) {
      if (elm) {
        if (i === 0) {
          url = url.concat("?".concat(elm));
        } else {
          url = url.concat("&".concat(elm));
        }
      }
    });
    router.push(url, undefined, {
      shallow: false
    });
  });
  return __jsx("div", {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 55,
      columnNumber: 5
    }
  }, "Filters:", __jsx("p", {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 57,
      columnNumber: 7
    }
  }, "Launch Year"), __jsx("div", {
    className: "row",
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 58,
      columnNumber: 7
    }
  }, YEARS && YEARS.map(function (year, i) {
    return __jsx("div", {
      key: i,
      className: "col-xs-6",
      __self: _this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 62,
        columnNumber: 15
      }
    }, __jsx(_atom_Button__WEBPACK_IMPORTED_MODULE_1__["default"], {
      className: "primary",
      label: year,
      action: "launch_year=".concat(year),
      type: "launch_year",
      handleClick: handleClick,
      __self: _this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 63,
        columnNumber: 17
      }
    }));
  })), __jsx("p", {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 73,
      columnNumber: 7
    }
  }, "Successful Launch"), __jsx("div", {
    className: "row",
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 74,
      columnNumber: 7
    }
  }, __jsx("div", {
    className: "col-xs-6",
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 75,
      columnNumber: 9
    }
  }, __jsx(_atom_Button__WEBPACK_IMPORTED_MODULE_1__["default"], {
    className: "primary",
    label: 'true',
    action: "launch_success=".concat(true),
    type: "launch_success",
    handleClick: handleClick,
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 76,
      columnNumber: 11
    }
  })), __jsx("div", {
    className: "col-xs-6",
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 78,
      columnNumber: 9
    }
  }, __jsx(_atom_Button__WEBPACK_IMPORTED_MODULE_1__["default"], {
    className: "secondary",
    label: 'false',
    action: "launch_success=".concat(false),
    type: "launch_success",
    handleClick: handleClick,
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 79,
      columnNumber: 11
    }
  }))), __jsx("p", {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 83,
      columnNumber: 7
    }
  }, "Successful landing"), __jsx("div", {
    className: "row",
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 84,
      columnNumber: 7
    }
  }, __jsx("div", {
    className: "col-xs-6",
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 85,
      columnNumber: 9
    }
  }, __jsx(_atom_Button__WEBPACK_IMPORTED_MODULE_1__["default"], {
    className: "primary",
    label: 'true',
    action: "land_success=".concat(true),
    type: "land_success",
    handleClick: handleClick,
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 86,
      columnNumber: 11
    }
  })), __jsx("div", {
    className: "col-xs-6",
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 88,
      columnNumber: 9
    }
  }, __jsx(_atom_Button__WEBPACK_IMPORTED_MODULE_1__["default"], {
    className: "secondary",
    label: 'false',
    action: "land_success=".concat(false),
    type: "land_success",
    handleClick: handleClick,
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 89,
      columnNumber: 11
    }
  }))));
};

_s(Filter, "TLPkP0vG33S7BRsBru/k6RZ8N/U=", false, function () {
  return [next_router__WEBPACK_IMPORTED_MODULE_2__["useRouter"]];
});

_c = Filter;
/* harmony default export */ __webpack_exports__["default"] = (Filter);

var _c;

$RefreshReg$(_c, "Filter");

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vY29tcG9uZW50cy9tb2xlY3VsZS9GaWx0ZXIuanMiXSwibmFtZXMiOlsiWUVBUlMiLCJGaWx0ZXIiLCJwcm9wcyIsInVzZVN0YXRlIiwic2VsZWN0ZWRZZWFyIiwic2V0U2VsZWN0ZWRZZWFyIiwic2VsZWN0ZWRMYXVuY2giLCJzZXRTZWxlY3RlZExhdW5jaCIsInNlbGVjdGVkTGFuZCIsInNldFNlbGVjdGVkbGFuZCIsInJvdXRlciIsInVzZVJvdXRlciIsImhhbmRsZUNsaWNrIiwiZGF0YSIsInR5cGUiLCJ1c2VFZmZlY3QiLCJ1cmxRdWVyeSIsInVybCIsIm1hcCIsImVsbSIsImkiLCJjb25jYXQiLCJwdXNoIiwidW5kZWZpbmVkIiwic2hhbGxvdyIsInllYXIiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQSxJQUFNQSxLQUFLLEdBQUcsQ0FDWixNQURZLEVBRVosTUFGWSxFQUdaLE1BSFksRUFJWixNQUpZLEVBS1osTUFMWSxFQU1aLE1BTlksRUFPWixNQVBZLEVBUVosTUFSWSxFQVNaLE1BVFksRUFVWixNQVZZLEVBV1osTUFYWSxFQVlaLE1BWlksRUFhWixNQWJZLEVBY1osTUFkWSxFQWVaLE1BZlksQ0FBZDs7QUFtQkEsSUFBTUMsTUFBTSxHQUFHLFNBQVRBLE1BQVMsQ0FBQ0MsS0FBRCxFQUFXO0FBQUE7O0FBQUEsa0JBQ2dCQyxzREFBUSxDQUFDLEVBQUQsQ0FEeEI7QUFBQSxNQUNqQkMsWUFEaUI7QUFBQSxNQUNIQyxlQURHOztBQUFBLG1CQUVvQkYsc0RBQVEsQ0FBQyxFQUFELENBRjVCO0FBQUEsTUFFakJHLGNBRmlCO0FBQUEsTUFFREMsaUJBRkM7O0FBQUEsbUJBR2dCSixzREFBUSxDQUFDLEVBQUQsQ0FIeEI7QUFBQSxNQUdqQkssWUFIaUI7QUFBQSxNQUdIQyxlQUhHOztBQUl4QixNQUFNQyxNQUFNLEdBQUdDLDZEQUFTLEVBQXhCOztBQUNBLE1BQU1DLFdBQVcsR0FBRyxTQUFkQSxXQUFjLENBQUNDLElBQUQsRUFBT0MsSUFBUCxFQUFnQjtBQUNsQyxRQUFJQSxJQUFJLEtBQUssYUFBYixFQUE0QjtBQUMxQlQscUJBQWUsQ0FBQ1EsSUFBRCxDQUFmO0FBQ0QsS0FGRCxNQUVPLElBQUlDLElBQUksS0FBSyxnQkFBYixFQUErQjtBQUNwQ1AsdUJBQWlCLENBQUNNLElBQUQsQ0FBakI7QUFDRCxLQUZNLE1BRUE7QUFDTEoscUJBQWUsQ0FBQ0ksSUFBRCxDQUFmO0FBQ0Q7QUFFRixHQVREOztBQVVBRSx5REFBUyxDQUFDLFlBQU07QUFDZCxRQUFJQyxRQUFRLEdBQUcsQ0FBQ1osWUFBRCxFQUFlRSxjQUFmLEVBQStCRSxZQUEvQixDQUFmO0FBQ0EsUUFBSVMsR0FBRyxHQUFHLEdBQVY7QUFDQUQsWUFBUSxJQUFJQSxRQUFRLENBQUNFLEdBQVQsQ0FBYSxVQUFDQyxHQUFELEVBQU1DLENBQU4sRUFBWTtBQUNuQyxVQUFJRCxHQUFKLEVBQVM7QUFDUCxZQUFJQyxDQUFDLEtBQUssQ0FBVixFQUFhO0FBQ1hILGFBQUcsR0FBR0EsR0FBRyxDQUFDSSxNQUFKLFlBQWVGLEdBQWYsRUFBTjtBQUNELFNBRkQsTUFFTztBQUNMRixhQUFHLEdBQUdBLEdBQUcsQ0FBQ0ksTUFBSixZQUFlRixHQUFmLEVBQU47QUFDRDtBQUNGO0FBRUYsS0FUVyxDQUFaO0FBVUFULFVBQU0sQ0FBQ1ksSUFBUCxDQUFZTCxHQUFaLEVBQWlCTSxTQUFqQixFQUE0QjtBQUFFQyxhQUFPLEVBQUU7QUFBWCxLQUE1QjtBQUNELEdBZFEsQ0FBVDtBQWdCQSxTQUNFO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsaUJBRUU7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQkFGRixFQUdFO0FBQUssYUFBUyxFQUFDLEtBQWY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxLQUVJeEIsS0FBSyxJQUFJQSxLQUFLLENBQUNrQixHQUFOLENBQVUsVUFBQ08sSUFBRCxFQUFPTCxDQUFQLEVBQWE7QUFDOUIsV0FDRTtBQUFLLFNBQUcsRUFBRUEsQ0FBVjtBQUFhLGVBQVMsRUFBQyxVQUF2QjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLE9BQ0UsTUFBQyxvREFBRDtBQUNFLGVBQVMsRUFBQyxTQURaO0FBRUUsV0FBSyxFQUFFSyxJQUZUO0FBRWUsWUFBTSx3QkFBaUJBLElBQWpCLENBRnJCO0FBR0UsVUFBSSxFQUFDLGFBSFA7QUFHcUIsaUJBQVcsRUFBRWIsV0FIbEM7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxNQURGLENBREY7QUFRRCxHQVRRLENBRmIsQ0FIRixFQWtCRTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHlCQWxCRixFQW1CRTtBQUFLLGFBQVMsRUFBQyxLQUFmO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsS0FDRTtBQUFLLGFBQVMsRUFBQyxVQUFmO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsS0FDRSxNQUFDLG9EQUFEO0FBQVEsYUFBUyxFQUFDLFNBQWxCO0FBQTRCLFNBQUssRUFBRSxNQUFuQztBQUEyQyxVQUFNLDJCQUFvQixJQUFwQixDQUFqRDtBQUE2RSxRQUFJLEVBQUMsZ0JBQWxGO0FBQW1HLGVBQVcsRUFBRUEsV0FBaEg7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxJQURGLENBREYsRUFJRTtBQUFLLGFBQVMsRUFBQyxVQUFmO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsS0FDRSxNQUFDLG9EQUFEO0FBQVEsYUFBUyxFQUFDLFdBQWxCO0FBQThCLFNBQUssRUFBRSxPQUFyQztBQUE4QyxVQUFNLDJCQUFvQixLQUFwQixDQUFwRDtBQUFpRixRQUFJLEVBQUMsZ0JBQXRGO0FBQXVHLGVBQVcsRUFBRUEsV0FBcEg7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxJQURGLENBSkYsQ0FuQkYsRUE0QkU7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSwwQkE1QkYsRUE2QkU7QUFBSyxhQUFTLEVBQUMsS0FBZjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEtBQ0U7QUFBSyxhQUFTLEVBQUMsVUFBZjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEtBQ0UsTUFBQyxvREFBRDtBQUFRLGFBQVMsRUFBQyxTQUFsQjtBQUE0QixTQUFLLEVBQUUsTUFBbkM7QUFBMkMsVUFBTSx5QkFBa0IsSUFBbEIsQ0FBakQ7QUFBMkUsUUFBSSxFQUFDLGNBQWhGO0FBQWdHLGVBQVcsRUFBRUEsV0FBN0c7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxJQURGLENBREYsRUFJRTtBQUFLLGFBQVMsRUFBQyxVQUFmO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsS0FDRSxNQUFDLG9EQUFEO0FBQVEsYUFBUyxFQUFDLFdBQWxCO0FBQThCLFNBQUssRUFBRSxPQUFyQztBQUE4QyxVQUFNLHlCQUFrQixLQUFsQixDQUFwRDtBQUErRSxRQUFJLEVBQUMsY0FBcEY7QUFBb0csZUFBVyxFQUFFQSxXQUFqSDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLElBREYsQ0FKRixDQTdCRixDQURGO0FBd0NELENBdkVEOztHQUFNWCxNO1VBSVdVLHFEOzs7S0FKWFYsTTtBQXlFU0EscUVBQWYiLCJmaWxlIjoic3RhdGljL3dlYnBhY2svcGFnZXMvX2FwcC4yMWNkMWYyZmZiODE1MGRkZmE4My5ob3QtdXBkYXRlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgdXNlU3RhdGUsIHVzZUVmZmVjdCB9IGZyb20gJ3JlYWN0JztcclxuaW1wb3J0IEJ1dHRvbiBmcm9tIFwiLi4vYXRvbS9CdXR0b25cIjtcclxuaW1wb3J0IHsgdXNlUm91dGVyIH0gZnJvbSAnbmV4dC9yb3V0ZXInO1xyXG5jb25zdCBZRUFSUyA9IFtcclxuICAnMjAwNicsXHJcbiAgJzIwMDcnLFxyXG4gICcyMDA4JyxcclxuICAnMjAwOScsXHJcbiAgJzIwMTAnLFxyXG4gICcyMDExJyxcclxuICAnMjAxMicsXHJcbiAgJzIwMTMnLFxyXG4gICcyMDE0JyxcclxuICAnMjAxNScsXHJcbiAgJzIwMTYnLFxyXG4gICcyMDE3JyxcclxuICAnMjAxOCcsXHJcbiAgJzIwMTknLFxyXG4gICcyMDIwJyxcclxuXTtcclxuXHJcblxyXG5jb25zdCBGaWx0ZXIgPSAocHJvcHMpID0+IHtcclxuICBjb25zdCBbc2VsZWN0ZWRZZWFyLCBzZXRTZWxlY3RlZFllYXJdID0gdXNlU3RhdGUoJycpO1xyXG4gIGNvbnN0IFtzZWxlY3RlZExhdW5jaCwgc2V0U2VsZWN0ZWRMYXVuY2hdID0gdXNlU3RhdGUoJycpO1xyXG4gIGNvbnN0IFtzZWxlY3RlZExhbmQsIHNldFNlbGVjdGVkbGFuZF0gPSB1c2VTdGF0ZSgnJyk7XHJcbiAgY29uc3Qgcm91dGVyID0gdXNlUm91dGVyKCk7XHJcbiAgY29uc3QgaGFuZGxlQ2xpY2sgPSAoZGF0YSwgdHlwZSkgPT4ge1xyXG4gICAgaWYgKHR5cGUgPT09ICdsYXVuY2hfeWVhcicpIHtcclxuICAgICAgc2V0U2VsZWN0ZWRZZWFyKGRhdGEpO1xyXG4gICAgfSBlbHNlIGlmICh0eXBlID09PSAnbGF1bmNoX3N1Y2Nlc3MnKSB7XHJcbiAgICAgIHNldFNlbGVjdGVkTGF1bmNoKGRhdGEpO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgc2V0U2VsZWN0ZWRsYW5kKGRhdGEpO1xyXG4gICAgfVxyXG4gICAgXHJcbiAgfVxyXG4gIHVzZUVmZmVjdCgoKSA9PiB7XHJcbiAgICBsZXQgdXJsUXVlcnkgPSBbc2VsZWN0ZWRZZWFyLCBzZWxlY3RlZExhdW5jaCwgc2VsZWN0ZWRMYW5kXTtcclxuICAgIGxldCB1cmwgPSBcIi9cIjtcclxuICAgIHVybFF1ZXJ5ICYmIHVybFF1ZXJ5Lm1hcCgoZWxtLCBpKSA9PiB7XHJcbiAgICAgIGlmIChlbG0pIHtcclxuICAgICAgICBpZiAoaSA9PT0gMCkge1xyXG4gICAgICAgICAgdXJsID0gdXJsLmNvbmNhdChgPyR7ZWxtfWApO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICB1cmwgPSB1cmwuY29uY2F0KGAmJHtlbG19YCk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICAgIFxyXG4gICAgfSlcclxuICAgIHJvdXRlci5wdXNoKHVybCwgdW5kZWZpbmVkLCB7IHNoYWxsb3c6IGZhbHNlIH0pXHJcbiAgfSk7XHJcblxyXG4gIHJldHVybiAoXHJcbiAgICA8ZGl2PlxyXG4gICAgICBGaWx0ZXJzOlxyXG4gICAgICA8cD5MYXVuY2ggWWVhcjwvcD5cclxuICAgICAgPGRpdiBjbGFzc05hbWU9J3Jvdyc+XHJcbiAgICAgICAge1xyXG4gICAgICAgICAgWUVBUlMgJiYgWUVBUlMubWFwKCh5ZWFyLCBpKSA9PiB7XHJcbiAgICAgICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgICAgPGRpdiBrZXk9e2l9IGNsYXNzTmFtZT0nY29sLXhzLTYnPlxyXG4gICAgICAgICAgICAgICAgPEJ1dHRvbiBcclxuICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPSdwcmltYXJ5JyBcclxuICAgICAgICAgICAgICAgICAgbGFiZWw9e3llYXJ9IGFjdGlvbj17YGxhdW5jaF95ZWFyPSR7eWVhcn1gfSBcclxuICAgICAgICAgICAgICAgICAgdHlwZT0nbGF1bmNoX3llYXInIGhhbmRsZUNsaWNrPXtoYW5kbGVDbGlja30vPlxyXG4gICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICApXHJcbiAgICAgICAgICB9KVxyXG4gICAgICAgIH1cclxuICAgICAgPC9kaXY+XHJcblxyXG4gICAgICA8cD5TdWNjZXNzZnVsIExhdW5jaDwvcD5cclxuICAgICAgPGRpdiBjbGFzc05hbWU9J3Jvdyc+XHJcbiAgICAgICAgPGRpdiBjbGFzc05hbWU9J2NvbC14cy02Jz5cclxuICAgICAgICAgIDxCdXR0b24gY2xhc3NOYW1lPSdwcmltYXJ5JyBsYWJlbD17J3RydWUnfSBhY3Rpb249e2BsYXVuY2hfc3VjY2Vzcz0ke3RydWV9YH0gdHlwZT0nbGF1bmNoX3N1Y2Nlc3MnIGhhbmRsZUNsaWNrPXtoYW5kbGVDbGlja30gLz5cclxuICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT0nY29sLXhzLTYnPlxyXG4gICAgICAgICAgPEJ1dHRvbiBjbGFzc05hbWU9J3NlY29uZGFyeScgbGFiZWw9eydmYWxzZSd9IGFjdGlvbj17YGxhdW5jaF9zdWNjZXNzPSR7ZmFsc2V9YH0gdHlwZT0nbGF1bmNoX3N1Y2Nlc3MnIGhhbmRsZUNsaWNrPXtoYW5kbGVDbGlja30vPlxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgICA8L2Rpdj5cclxuXHJcbiAgICAgIDxwPlN1Y2Nlc3NmdWwgbGFuZGluZzwvcD5cclxuICAgICAgPGRpdiBjbGFzc05hbWU9J3Jvdyc+XHJcbiAgICAgICAgPGRpdiBjbGFzc05hbWU9J2NvbC14cy02Jz5cclxuICAgICAgICAgIDxCdXR0b24gY2xhc3NOYW1lPSdwcmltYXJ5JyBsYWJlbD17J3RydWUnfSBhY3Rpb249e2BsYW5kX3N1Y2Nlc3M9JHt0cnVlfWB9IHR5cGU9J2xhbmRfc3VjY2VzcycgIGhhbmRsZUNsaWNrPXtoYW5kbGVDbGlja30vPlxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPSdjb2wteHMtNic+XHJcbiAgICAgICAgICA8QnV0dG9uIGNsYXNzTmFtZT0nc2Vjb25kYXJ5JyBsYWJlbD17J2ZhbHNlJ30gYWN0aW9uPXtgbGFuZF9zdWNjZXNzPSR7ZmFsc2V9YH0gdHlwZT0nbGFuZF9zdWNjZXNzJyAgaGFuZGxlQ2xpY2s9e2hhbmRsZUNsaWNrfS8+XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICAgIDwvZGl2PlxyXG4gICAgPC9kaXY+XHJcbiAgKVxyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBGaWx0ZXI7XHJcblxyXG4iXSwic291cmNlUm9vdCI6IiJ9