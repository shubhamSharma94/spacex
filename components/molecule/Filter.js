import { useState, useEffect } from 'react';
import Button from "../atom/Button";
import { useRouter } from 'next/router';
const YEARS = [
  '2006',
  '2007',
  '2008',
  '2009',
  '2010',
  '2011',
  '2012',
  '2013',
  '2014',
  '2015',
  '2016',
  '2017',
  '2018',
  '2019',
  '2020',
];


const Filter = (props) => {
  const [selectedYear, setSelectedYear] = useState('');
  const [selectedLaunch, setSelectedLaunch] = useState('');
  const [selectedLand, setSelectedland] = useState('');
  const router = useRouter();
  const handleClick = (data, type) => {
    if (type === 'launch_year') {
      setSelectedYear(data);
    } else if (type === 'launch_success') {
      setSelectedLaunch(data);
    } else {
      setSelectedland(data);
    }
    
  }
  useEffect(() => {
    let urlQuery = [selectedYear, selectedLaunch, selectedLand];
    let url = "/";
    urlQuery && urlQuery.map((elm, i) => {
      if (elm) {
        if (i === 0) {
          url = url.concat(`?${elm}`);
        } else {
          url = url.concat(`&${elm}`);
        }
      }
      
    })
    router.push(url, undefined, { shallow: false })
  });

  return (
    <div>
      Filters:
      <p>Launch Year</p>
      <div className='row'>
        {
          YEARS && YEARS.map((year, i) => {
            return (
              <div key={i} className='col-xs-6'>
                <Button 
                  className='primary' 
                  label={year} action={`launch_year=${year}`} 
                  type='launch_year' handleClick={handleClick}/>
              </div>
            )
          })
        }
      </div>

      <p>Successful Launch</p>
      <div className='row'>
        <div className='col-xs-6'>
          <Button className='primary' label={'true'} action={`launch_success=${true}`} type='launch_success' handleClick={handleClick} />
        </div>
        <div className='col-xs-6'>
          <Button className='secondary' label={'false'} action={`launch_success=${false}`} type='launch_success' handleClick={handleClick}/>
        </div>
      </div>

      <p>Successful landing</p>
      <div className='row'>
        <div className='col-xs-6'>
          <Button className='primary' label={'true'} action={`land_success=${true}`} type='land_success'  handleClick={handleClick}/>
        </div>
        <div className='col-xs-6'>
          <Button className='secondary' label={'false'} action={`land_success=${false}`} type='land_success'  handleClick={handleClick}/>
        </div>
      </div>
    </div>
  )
}

export default Filter;

