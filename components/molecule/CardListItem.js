import styled from 'styled-components';

const style = {
  wrapper: {
  },
  image: {
    height: '210px',
    backgroundSize: 'contain',
    backgroundRepeat: 'no-repeat',
  },
  title: {
    fontWeight: "700",
    marginTop: '16px',
    color: 'darkblue',
  },

};

const Wrapper = styled('div')(style.wrapper);
const Image = styled('div')(style.image);
const Title = styled('p')(style.title);

const CardListItem = (props) => {
  return (
    <Wrapper className='col-xs-12 col-sm-6 col-md-3'>
      <Image style={{backgroundImage: `url(${props.links.mission_patch_small})`}}></Image>
      <Title>
        {props.mission_name} # {props.flight_number}
      </Title>
      <div className='row'>
        <label className='col-xs-6'>Mission Ids</label>
        <ul className='col-xs-6'>{props.mission_id && props.mission_id.map((id) => (
          <li key={id}>{id}</li>
        ))}</ul>
      </div>
      <div className='row'>
        <label className='col-xs-6'>Launch Year</label>
        <span className='col-xs-6'>{props.launch_year}</span>
      </div>
      <div className='row'>
        <label className='col-xs-6'>Successful Launch</label>
        <span className='col-xs-6'>{props.launch_success ? 'Yes' : 'No'}</span>
      </div>
      <div className='row'>
        <label className='col-xs-6'>Successful Landing</label>
        <span className='col-xs-6'>{props.rocket.first_stage.cores[0].land_success ? 'Yes' : 'No'}</span>
      </div>
    </Wrapper>
  );
};

export default CardListItem;
