import styled from 'styled-components';

const style = {
  heading: {
    textAlign: 'center'
  }
};

const StyledHeading = styled('h1')(style.heading);

const Heading = (props) => {
  return (
    <StyledHeading>{props.heading}</StyledHeading>
  )
}

export default Heading;