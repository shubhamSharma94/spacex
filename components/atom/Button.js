import styled from 'styled-components';
import { useRouter } from 'next/router';

const style = {
  wrapper: {
    marginBottom: '8px',
    'button': {
      textTransform: 'uppercase',
    }
  }
};

const Wrapper = styled('div')(style.wrapper);

const Button = (props) => {
  const router = useRouter();
  const {launch_year, launch_success, land_success} = router.query

  return (
    <Wrapper>
      <button 
        onClick={() => {props.handleClick(props.action, props.type)}} 
        className={`btn btn-${props.className} btn-${(props.label == launch_year || (props.label == launch_success && props.type == 'launch_success') || (props.label == land_success  && props.type == 'land_success')) ? 'warning' : ''}`}
      >{props.label}</button>
    </Wrapper>
  )
}

export default Button;