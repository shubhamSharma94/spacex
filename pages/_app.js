import "../assets/css/bootstrap.min.css";
import Index from "./index";

export default function MyApp({ Component, pageProps }) {
  return <Index {...pageProps} />
}
