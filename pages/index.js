import { useState, useEffect } from "react";
import fetch from "isomorphic-unfetch";
import Heading from "../components/atom/heading";
import CardListItem from "../components/molecule/CardListItem";
import styled from "styled-components";
import { device } from "../utils/mediaQueries/device";
import Filter from "../components/molecule/Filter";

const style = {
  container: {
    [device.laptopL]: {
      width: "1440px",
      margin: "auto",
    },
  },
};

const Container = styled("div")(style.container);

const Index = (props) => {
  const { list } = props;


  return (
    <Container className="container">
      <Heading heading="SpaceX Launch Programs" />
      <div className="row">
        <div className="col-xs-12 col-sm-3">
        <Filter />

        </div>
        <div className="col-xs-12 col-sm-9">
          <div className="row">
            {list &&
              list.map((elm) => {
                return <CardListItem key={elm.flight_number} {...elm} />;
              })}
          </div>
        </div>
      </div>
    </Container>
  );
};

export default Index;

Index.getInitialProps = async ({ query }) => {
  const {launch_year, launch_success, land_success} = query;
  const url = `https://api.spacexdata.com/v3/launches?limit=100${launch_year ? '&launch_year=' + launch_year : ''}${launch_success ? '&launch_success=' + launch_success : ''}${land_success ? '&land_success=' + land_success : ''}`

  const res = await fetch(url);
  const data = await res.json();

  return {
    list: data,
  };
};
